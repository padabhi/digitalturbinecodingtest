package com.pratikdabhi.digitalturbinecodingtest.interfaces;

import com.pratikdabhi.digitalturbinecodingtest.fragments.TestFragment;

/**
 * Created by pratikdabhi on 12/6/16.
 */

public interface ActivityCallbacks {
    void setFragment(TestFragment fragment, boolean addToBackStack);
    void setTitle(String title, boolean upAsBack);
}
