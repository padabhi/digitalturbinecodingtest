package com.pratikdabhi.digitalturbinecodingtest.fragments;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v4.content.res.ResourcesCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.pratikdabhi.digitalturbinecodingtest.Ad;
import com.pratikdabhi.digitalturbinecodingtest.IconLoadTask;
import com.pratikdabhi.digitalturbinecodingtest.R;

/**
 * Created by pratikdabhi on 12/6/16.
 */

public class AdDetailFragment extends TestFragment {

    private static final String TAG = "AdDetailFragment";
    private static final String EXTRA_PRODUCT = "product";
    private static final String EXTRA_SHOWING_MORE = "showing_more";
    private Ad mAd;
    private Button mShowMore;
    private boolean mShowingMore = false;

    private static final int[] hiddenIds = new int[] {
            R.id.app_id,
            R.id.app_id_label,
            R.id.bidrate,
            R.id.bidrate_label,
            R.id.campaign_id,
            R.id.campaign_id_label,
            R.id.campaign_type_id,
            R.id.campaign_type_id_label,
            R.id.campaign_display_order,
            R.id.campaign_display_order_label,
            R.id.creative_id,
            R.id.creative_id_label,
            R.id.homescreen,
            R.id.homescreen_label,
            R.id.random_pick,
            R.id.random_pick_label,
            R.id.impressions_url
    };

    public static AdDetailFragment getInstance(@NonNull Ad ad) {
        AdDetailFragment fragment = new AdDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_PRODUCT, ad);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() == null || !getArguments().containsKey(EXTRA_PRODUCT))
            throw new IllegalArgumentException("Arguments must contain a Ad!");

        mAd = getArguments().getParcelable(EXTRA_PRODUCT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View main = inflater.inflate(R.layout.ad_detail_fragment, container, false);
        setText(main, R.id.name, mAd.mName);
        setText(main, R.id.description, mAd.mDescription);
        setText(main, R.id.rating, getString(R.string.stars, mAd.mRating));
        setText(main, R.id.num_ratings, mAd.mNumberOfRatings);
        setText(main, R.id.min_version, mAd.mMinVersion);
        setText(main, R.id.category, mAd.mCategory);
        setText(main, R.id.action, mAd.mAction);
        setText(main, R.id.bidrate, mAd.mBidRate);
        setText(main, R.id.app_id, mAd.mAppId);
        setText(main, R.id.creative_id, mAd.mCreativeId);
        setText(main, R.id.campaign_id, mAd.mCampaignId);
        setText(main, R.id.campaign_type_id, mAd.mCampaignTypeId);
        setText(main, R.id.campaign_display_order, mAd.mCampaignDisplayOrder);
        setText(main, R.id.homescreen, String.valueOf(mAd.mHomeScreen));
        setText(main, R.id.random_pick, String.valueOf(mAd.mIsRandomPick));

        mShowMore = (Button) main.findViewById(R.id.show_more);
        mShowMore.setOnClickListener(v -> {
            mShowingMore = !mShowingMore;
            showMoreInfo(getView());
        });

        if (savedInstanceState != null) mShowingMore = savedInstanceState.getBoolean(EXTRA_SHOWING_MORE, false);

        showMoreInfo(main);

        main.findViewById(R.id.impressions_url).setOnClickListener(v -> {
            impressionsUrl();
        });

        main.findViewById(R.id.action).setOnClickListener(v -> {
            proxyUrl();
        });

        ImageView thumbnail = (ImageView) main.findViewById(R.id.thumbnail);
        if (IconLoadTask.cancelPotentialWork(mAd.mThumbnailUrl, thumbnail)) {
            final IconLoadTask task = new IconLoadTask(thumbnail,
                    mAd.mThumbnailUrl);
            final IconLoadTask.AsyncDrawable asyncDrawable =
                    new IconLoadTask.AsyncDrawable(task);
            thumbnail.setImageDrawable(asyncDrawable);
            task.execute();
        }

        ImageView ratingImage = (ImageView) main.findViewById(R.id.rating_image);
        if (IconLoadTask.cancelPotentialWork(mAd.mRatingPic, ratingImage)) {
            final IconLoadTask task = new IconLoadTask(ratingImage,
                    mAd.mRatingPic);
            final IconLoadTask.AsyncDrawable asyncDrawable =
                    new IconLoadTask.AsyncDrawable(task);
            ratingImage.setImageDrawable(asyncDrawable);
            task.execute();
        }
        return main;
    }

    private void setText(View main, int id, String text) {
        ((TextView) main.findViewById(id)).setText(text);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelable(EXTRA_PRODUCT, mAd);
        outState.putBoolean(EXTRA_SHOWING_MORE, mShowingMore);
        super.onSaveInstanceState(outState);
    }

    @Override
    boolean hasUpAsBack() {
        return true;
    }

    @Override
    public String getTagName() {
        return TAG;
    }

    @Override
    String getTitle() {
        return mAd.mName;
    }

    public void showMoreInfo(@NonNull View v) {
        mShowMore.setText(mShowingMore ? R.string.show_less_info : R.string.show_more_info);
        for (int id : hiddenIds) {
            v.findViewById(id).setVisibility(mShowingMore ? View.VISIBLE : View.GONE);
        }
    }

    public void impressionsUrl() {
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(ResourcesCompat.getColor(getResources(), R.color.colorPrimary, getActivity().getTheme()));
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(getActivity(), Uri.parse(mAd.mImpressionTrackingUrl));
    }

    public void proxyUrl() {
        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
        builder.setToolbarColor(ResourcesCompat.getColor(getResources(), R.color.colorPrimary, getActivity().getTheme()));
        CustomTabsIntent customTabsIntent = builder.build();
        customTabsIntent.launchUrl(getActivity(), Uri.parse(mAd.mClickProxyUrl));
    }
}
