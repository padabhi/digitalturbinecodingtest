package com.pratikdabhi.digitalturbinecodingtest.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pratikdabhi.digitalturbinecodingtest.IconLoadTask;
import com.pratikdabhi.digitalturbinecodingtest.Ad;
import com.pratikdabhi.digitalturbinecodingtest.AdLoader;
import com.pratikdabhi.digitalturbinecodingtest.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pratikdabhi on 12/6/16.
 */

public class AdListFragment extends TestFragment implements LoaderManager.LoaderCallbacks<List<Ad>>, View.OnClickListener {

    private static final String TAG = "AdListFragment";
    private static final String EXTRA_PRODUCTS = "ads";
    private static final int LOADER_ID = 1234;
    private TextView mEmpty;
    private ProgressBar mSpinner;
    private SwipeRefreshLayout mSwipeRefresh;
    private final ProductAdapter mAdapter = new ProductAdapter();
    ArrayList<Ad> mData = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View main = inflater.inflate(R.layout.ad_list_fragment, container, false);
        mEmpty = (TextView) main.findViewById(R.id.empty);
        mSpinner = (ProgressBar) main.findViewById(R.id.progress);
        mSwipeRefresh = (SwipeRefreshLayout) main.findViewById(R.id.swipe_layout);
        mSwipeRefresh.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
        mSwipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                getLoaderManager().restartLoader(LOADER_ID, null, AdListFragment.this);
            }
        });
        RecyclerView recyclerView = (RecyclerView) main.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                mSwipeRefresh.setEnabled(((LinearLayoutManager)recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition() == 0);
            }
        });
        if (savedInstanceState != null && savedInstanceState.containsKey(EXTRA_PRODUCTS)) {
            setData(savedInstanceState.getParcelableArrayList(EXTRA_PRODUCTS));
        }
        if (mData.isEmpty()) {
            getLoaderManager().restartLoader(LOADER_ID, null, this);
        } else {
            mSpinner.setVisibility(View.GONE);
            mEmpty.setVisibility(View.GONE);
            mSwipeRefresh.setVisibility(View.VISIBLE);
        }
        return main;
    }

    @Override
    public boolean hasUpAsBack() {
        return false;
    }

    @Override
    public String getTagName() {
        return TAG;
    }

    @Override
    String getTitle() {
        return getString(R.string.ads);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(EXTRA_PRODUCTS, mData);
        super.onSaveInstanceState(outState);
    }

    private void setData(List<Ad> data) {
        mData.clear();
        mData.addAll(data);
        mAdapter.notifyDataSetChanged();
        mSpinner.setVisibility(View.GONE);
        if (data.isEmpty()) {
            mEmpty.setVisibility(View.VISIBLE);
            mSwipeRefresh.setVisibility(View.GONE);
        } else {
            mEmpty.setVisibility(View.GONE);
            mSwipeRefresh.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public Loader<List<Ad>> onCreateLoader(int id, Bundle args) {
        return new AdLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<List<Ad>> loader, List<Ad> data) {
        if (getActivity() == null) return;
        mSwipeRefresh.setRefreshing(false);
        getLoaderManager().destroyLoader(loader.getId());
        setData(data);
    }

    @Override
    public void onLoaderReset(Loader<List<Ad>> loader) {
    }

    @Override
    public void onClick(View view) {
        Ad ad = (Ad) view.getTag();
        if (mCallbacks != null) mCallbacks.setFragment(AdDetailFragment.getInstance(ad), true);
    }

    private class ProductAdapter extends RecyclerView.Adapter<ProductViewHolder> {

        @Override
        public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(getActivity()).inflate(R.layout.ad_list_item, parent, false);
            return new ProductViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ProductViewHolder holder, int position) {
            Ad ad = mData.get(position);
            holder.itemView.setTag(ad);
            holder.mName.setText(ad.mName);
            holder.mRating.setText(getString(R.string.stars, ad.mRating));
            if (IconLoadTask.cancelPotentialWork(ad.mThumbnailUrl, holder.mThumbnail)) {
                final IconLoadTask task = new IconLoadTask(holder.mThumbnail,
                        ad.mThumbnailUrl);
                final IconLoadTask.AsyncDrawable asyncDrawable =
                        new IconLoadTask.AsyncDrawable(task);
                holder.mThumbnail.setImageDrawable(asyncDrawable);
                task.execute();
            }
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }
    }

    private class ProductViewHolder extends RecyclerView.ViewHolder {
        ImageView mThumbnail;
        TextView mName;
        TextView mRating;

        public ProductViewHolder(View itemView) {
            super(itemView);
            mThumbnail = (ImageView) itemView.findViewById(R.id.thumbnail);
            mName = (TextView) itemView.findViewById(R.id.name);
            mRating = (TextView) itemView.findViewById(R.id.rating);
            itemView.setOnClickListener(AdListFragment.this);
        }
    }


}
